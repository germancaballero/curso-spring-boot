import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.curso.cfg.Configuracion;
import com.curso.modelo.entidad.Cliente;
import com.curso.modelo.entidad.DetallePedido;
import com.curso.modelo.entidad.Pedido;
import com.curso.modelo.entidad.Producto;

public class CargaDatos {

	public static void main(String[] args) {
		
		ApplicationContext appCtx = new AnnotationConfigApplicationContext(Configuracion.class);
		
		EntityManagerFactory emf = (EntityManagerFactory) appCtx.getBean("entityManagerFactory");
		EntityManager em = emf.createEntityManager();
		
		em.getTransaction().begin();
		Producto p1 = new Producto(null, "Producto 1", "Un fabricante", 25.5, 1000);
		Producto p2 = new Producto(null, "Producto 2", "Fabricante 2do", 11.1, 2000);
		em.persist(p1);
		em.persist(p2);
		
		Cliente c1 = new Cliente(null, "Bart", "Springfield", "1231232", 4);		
		Cliente c2 = new Cliente(null, "Maggie", "Springfield", "1231232", 4);
		em.persist(c1);
		em.persist(c2);
		
		Pedido pedido1 = new Pedido(null, "PED-0", LocalDate.now(), "PENDIENTE", c1, null );
		List<DetallePedido> detalles1 = new ArrayList<DetallePedido>();
		detalles1.add(new DetallePedido(null, pedido1, p2, p2.getPrecio(), 33));
		detalles1.add(new DetallePedido(null, pedido1, p1, p1.getPrecio(), 22));
		pedido1.setDetalles(detalles1);
		
		em.persist(pedido1);
		// No persistimos los detalles, por configuración JPA haremos que se se persistan 
		// de manera automática
		
		em.getTransaction().commit();
		em.close();
		emf.close();
	}	
}
