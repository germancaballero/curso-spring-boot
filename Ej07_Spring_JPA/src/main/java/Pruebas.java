import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.curso.cfg.Configuracion;
import com.curso.modelo.entidad.Cliente;
import com.curso.modelo.negocio.GestorClientes;

public class Pruebas {

	public static void main(String[] args) {

		//Sin Spring:
		//EntityManagerFactory emf = Persistence.createEntityManagerFactory("h2PU");
		//EntityManager em = emf.createEntityManager();
		//...
		//em.close();
		
		ApplicationContext appCtx = new AnnotationConfigApplicationContext(Configuracion.class);
		
		GestorClientes gc = appCtx.getBean(GestorClientes.class);

		Cliente c1 = new Cliente(null, "Menganito", "Madrid", "1231232", 4);	
		gc.insertar(c1);
		
		List<Cliente> todos = gc.listar();
		todos.forEach(c -> System.out.println(c.getNombre()));
	}
	
}










