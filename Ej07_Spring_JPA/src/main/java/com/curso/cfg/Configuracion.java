package com.curso.cfg;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.hibernate.cfg.Environment;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@ComponentScan(basePackages = "com.curso")
@EnableTransactionManagement
public class Configuracion {

	@Bean
	public DataSource dataSource() {
		DriverManagerDataSource ds = new DriverManagerDataSource();
		ds.setDriverClassName("org.h2.Driver");
		ds.setUrl("jdbc:h2:file:c:/h2/bbbb_pedidos");
		ds.setUsername("sa");
		ds.setPassword("");
		return ds;
	}

	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource ds) {
		LocalContainerEntityManagerFactoryBean emfBean = new LocalContainerEntityManagerFactoryBean();
		emfBean.setDataSource(ds);
		emfBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
		emfBean.setPackagesToScan("com.curso.modelo.entidad");
		/*
		if (emfBean instanceof FactoryBean) {
			FactoryBean newObj = (FactoryBean) emfBean;
			return newObj.getObject();
		}*/
		
		Properties jpaProp = new Properties();
		jpaProp.put("hibernate.dialect", "org.hibernate.dialect.H2Dialect");
		jpaProp.put("hibernate.show_sql", "true");
		jpaProp.put("hibernate.format_sql", "false");
		// DDL =  lenguaje de base de datos: CREATE TABLE, ALTER TABLE, etc
		// Hibernate hacia DDL, de manera automática actualice la bbdd
		// Es decir, crea la las tablas de la bbdd si estas no existe
		// También modifica las tablas siempre que se pueda
		jpaProp.put("hibernate.hbm2ddl.auto", "update");
		
		emfBean.setJpaProperties(jpaProp);
		
		return  emfBean;
	}
	
	@Bean
	public JpaTransactionManager transactionManager(EntityManagerFactory emf) {
		JpaTransactionManager tm = new JpaTransactionManager();
		tm.setEntityManagerFactory(emf);
		return tm;
	}
}
