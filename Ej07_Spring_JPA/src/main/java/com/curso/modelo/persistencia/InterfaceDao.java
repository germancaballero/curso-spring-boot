package com.curso.modelo.persistencia;

import java.util.List;

// T: Tipo del objeto para persistir, y K el tipo de la clave primaria (Id)
public interface InterfaceDao<T, K> {

	void insertar(T obj);
	void modificar(T obj);
	void borrar(T obj);
	T buscar(K id);
	List<T> listar();
}
