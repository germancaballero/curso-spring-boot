package com.curso.modelo.entidad;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "productos")
public class Producto {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(length = 40)
	private String nombre;
	private Double fabricante;
	private Double precio;
	@Column(name = "num_existencias")
	private Integer existencias;
	// De este campo no se crea una columna en bbdd
	@Transient
	private String datoQueNoQueremosPersistir;
	private transient String datoQueNoQueremosSerializarNiPersistir;

	public Producto() {
		super();
	}

	public Producto(Integer id, String nombre, String fabricante, Double precio, Integer existencias) {
		super();
		this.id = id;
		this.nombre = nombre;
		//this.fabricante = fabricante;
		this.fabricante = precio;
		this.precio = precio;
		this.existencias = existencias;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Double getFabricante() {
		return fabricante;
	}

	public void setFabricante(Double fabricante) {
		this.fabricante = fabricante;
	}

	public Double getPrecio() {
		return precio;
	}

	public void setPrecio(Double precio) {
		this.precio = precio;
	}

	public Integer getExistencias() {
		return existencias;
	}

	public void setExistencias(Integer existencias) {
		this.existencias = existencias;
	}

}
