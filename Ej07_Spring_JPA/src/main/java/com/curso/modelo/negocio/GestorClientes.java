package com.curso.modelo.negocio;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.curso.modelo.entidad.Cliente;
import com.curso.modelo.persistencia.ClienteDao;

@Service
// Lógica de negocio: Cualquier tipo de tratamiento de los datos, de procesado o de validación
public class GestorClientes {
	
	@Autowired
	private ClienteDao clienteDao;
	
	// Hacerlo en una sóla transacción
	@Transactional	// Lo usamos aquí en la lógica de negocio
	public void insertar(Cliente cliente) {
		if (cliente.getNombre().length() > 1) {
			cliente.setNombre(cliente.getNombre().toUpperCase());
			clienteDao.insertar(cliente);
		} else {
			// throw new Exception();
		}
	}
	// Hacerlo en una sóla transacción
	@Transactional	// Lo usamos aquí en la lógica de negocio
	public void insertar(List<Cliente> clientes) {
		for (Cliente cliente : clientes) {
			cliente.setNombre(cliente.getNombre().toUpperCase());
			clienteDao.insertar(cliente);
		}
	}
	
	public List<Cliente> listar() {
		return clienteDao.listar();
	}
}
