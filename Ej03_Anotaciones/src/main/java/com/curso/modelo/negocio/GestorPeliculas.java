package com.curso.modelo.negocio;

import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.curso.modelo.entidad.Pelicula;
import com.curso.modelo.persistencia.PeliculaDao;
import com.curso.util.Logger;

//Estereotipos
//@Service
//@Repository
//@Component
//@Controller
//@RestController

// @Service
//<bean id="gestorPeliculas" class="com.curso.modelo.negocio.GestorPeliculas" scope="singletón"/>

@Service("gestPeliculas")
//<bean id="gestPeliculas" class="com.curso.modelo.negocio.GestorPeliculas" scope="singletone"/>

//@Service()
@Scope("prototype")
//<bean id="gestorPeliculas" class="com.curso.modelo.negocio.GestorPeliculas" scope="prototype"/>

public class GestorPeliculas {

	@Autowired 	// Por defecto es por tipo
	private PeliculaDao peliculaDao;

	/*@Autowired
	@Qualifier("logger_")*/
	private Logger logger;
	
	/*@Autowired
	@Qualifier("loggerError_")*/
	private Logger loggerError;
	
	private GestorPeliculas() {
		super();
	}

	@Autowired 
	private GestorPeliculas(
			PeliculaDao peliculaDao,
			@Qualifier("logger_")   Logger logger,
			@Qualifier("loggerError_")
			Logger loggerError) {
		super();
		this.peliculaDao = peliculaDao;
		this.logger = logger;
		this.loggerError = loggerError;
	}


	/*
	public void setPeliculaDao(PeliculaDao peliculaDao) {
		this.peliculaDao = peliculaDao;
	}
	*/
	public void insertar(Pelicula pelicula) throws SQLException {
		peliculaDao.insertar(pelicula);
		logger.escribir("Pelicula insertada " + pelicula.toString());
		loggerError.escribir("Pelicula insertada " + pelicula.toString());
	}
}



