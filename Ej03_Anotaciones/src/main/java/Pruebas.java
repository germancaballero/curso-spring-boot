import java.sql.SQLException;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.curso.modelo.entidad.Pelicula;
import com.curso.modelo.negocio.GestorPeliculas;

public class Pruebas {

	public static void main(String[] args) throws SQLException {
		
		ApplicationContext appCtx = new ClassPathXmlApplicationContext("Beans.xml");

		GestorPeliculas gp = appCtx.getBean(GestorPeliculas.class);
		GestorPeliculas gp2 = (GestorPeliculas) appCtx.getBean("gestPeliculas");
		
		Pelicula p1 = new Pelicula(null, "Star Wars IV", "CiFi");
		gp.insertar(p1);
		if (gp == gp2) {
			System.out.println("Como es singleton es el mismo");
		} else {
			System.out.println("Como es prototype no es el mismo");
		}
	}
	
}

