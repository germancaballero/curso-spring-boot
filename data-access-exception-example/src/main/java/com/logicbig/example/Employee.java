package com.logicbig.example;

import org.springframework.context.annotation.ComponentScan;

public class Employee {
    private long id;
    private String name;
    private String dept;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", dept='" + dept + '\'' +
                '}';
    }

    public static Employee create(String name, String dept){
        Employee e = new Employee();
        e.setName(name);
        e.setDept(dept);
        return e;
    }
}