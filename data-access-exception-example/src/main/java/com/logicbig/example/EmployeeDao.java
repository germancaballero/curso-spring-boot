package com.logicbig.example;

import java.sql.SQLException;

public interface EmployeeDao {
	void save(Employee empleado) throws SQLException;
	// Employee findByDept(String dept);
}