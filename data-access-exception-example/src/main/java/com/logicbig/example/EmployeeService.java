package com.logicbig.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

@Service
public class EmployeeService {
    @Autowired
    EmployeeDao employeeDao;

    public void saveEmployee(Employee employee) {
        try {
            employeeDao.save(employee);
        } catch (DataAccessException dae) {
            System.err.println(dae);
        }
    }
}