package com.curso.util;

import java.io.BufferedWriter;
import java.io.FileWriter;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

//Para controlar el ciclo de vida tenemos dos maneras de hacerlo:
//-por interfaces
//-por configuración

//Vamos a necesitar más de un logger asi que no usamos arrobas
@Component
public class Logger implements InitializingBean, DisposableBean  {

	@Value("log_fich.txt")
	private String nombreFichero;
	private BufferedWriter bw;
	
	
	public Logger() {
		System.out.println("Logger:Constructor");
	}
	
	public void setNombreFichero(String nombreFichero) {
		System.out.println("Logger:SetNombreFichero");
		this.nombreFichero = nombreFichero;
	}	
	
	public synchronized void escribir(String texto) {
		
		try{
			bw.write(texto+"\n");
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		System.out.println("Logger:  afterPropertiesSet() ");
		FileWriter fw = new FileWriter(nombreFichero);
		bw = new BufferedWriter(fw);
	}

	@Override
	public void destroy() throws Exception {
		System.out.println("Logger: Destroy() ");
		// bw.flush(); Cuando se cierra ya se vuelva el buffer
		bw.close();
	}

	public void inicializar() throws Exception {
		System.out.println("Logger: inicializar");
		afterPropertiesSet();
	}

	public void finalizar() throws Exception {
		System.out.println("Logger: finalizar");
		destroy();
	}
	
	//Si no queremos usar las interfaces podemos crear los metodos e indicarlo en 
	//la configuración
	//<bean id="logger" 
	//      class="com.curso.util.Logger"
	//      init-method="inicializar"
	//      destroy-method="destruir">
	//	<property name="nombreFichero" value="log.txt"/>
	//</bean>	

	
	//Tambien se le puede pedir a Spring que busque las anotaciones
	//JEE para el ciclo de vida
	/*
	@PostConstruct
	public void inicializar2() throws Exception{
		System.out.println("Inicializando logger");
	}
	@PreDestroy
	public void destruir2() throws Exception{
		System.out.println("Muerte y destrucción");
	}
	*/
	
}
