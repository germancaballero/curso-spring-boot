package com.curso.modelo.negocio;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

//Esta bean tiene una referencia al contenedor de Spring
public class BeanQueSabeDondeVive implements InitializingBean, ApplicationContextAware {
	private ApplicationContext appContext;

	
	
	public BeanQueSabeDondeVive() {
		super();
		System.out.println("CONSTRUCTOER: Bean que sabe donde vive");
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		System.out.println("BeanQueSabeDondeVive: setApplicationContext()");
		this.appContext = applicationContext;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		System.out.println("BeanQueSabeDondeVive: afterPropertiesSet()");
		BeanQueSabeComoSeLlama bn = appContext.getBean(BeanQueSabeComoSeLlama.class);
		bn.saludar();
	}
}
