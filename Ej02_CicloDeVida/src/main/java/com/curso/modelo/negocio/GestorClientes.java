package com.curso.modelo.negocio;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.curso.util.Logger;

public class GestorClientes {

	private Logger logger;
	private Logger loggerError;

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public void setLoggerError(Logger loggerError) {
		this.loggerError = loggerError;
	}

	public void insertar(String cliente) {
		System.out.println("Insertando el cliente: " + cliente);
		logger.escribir("TODO BIEN " + cliente.toString());
		loggerError.escribir("ZASCA! " + cliente.toString());
	}

}
