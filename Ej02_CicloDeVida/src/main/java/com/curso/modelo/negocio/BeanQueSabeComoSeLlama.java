package com.curso.modelo.negocio;

import org.springframework.beans.factory.BeanNameAware;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

//Una bean que sabe cuál es si id/name
public class BeanQueSabeComoSeLlama implements InitializingBean, BeanNameAware  {

	private String m_nombre;
	
	public BeanQueSabeComoSeLlama() {
		super();
		System.out.println("Bean que sabe como se llama. CONSTRUCTOR");
		System.out.println("name:"+m_nombre);
	}
	public void saludar() {
		System.out.println("¡Hola! Soy "+m_nombre);
	}
	
	@Override
	public void setBeanName(String n) {
		this.m_nombre = n.toUpperCase();
		System.out.println("Bean que sabe como se llama: setBeanName() ");
		System.out.println("Nombre: " + n);
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		System.out.println("Bean que sabe como se llama: afterPropertiesSet() ");
		System.out.println("Nombre: " + m_nombre);
	}
	
	
}
