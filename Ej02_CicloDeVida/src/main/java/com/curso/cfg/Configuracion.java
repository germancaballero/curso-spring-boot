package com.curso.cfg;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.curso.util.Logger;

//Arrea! @Configuration es opcional, NMJ
//@Configuration
public class Configuracion {
	
	Logger logger() {
		Logger l = new Logger();
		return l;
	}
		
	Logger loggerError() {
		Logger l = new Logger();
		return l;
	}
	
}
