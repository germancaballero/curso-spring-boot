import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.curso.modelo.negocio.BeanQueSabeComoSeLlama;
import com.curso.modelo.negocio.BeanQueSabeDondeVive;
import com.curso.modelo.negocio.GestorClientes;

public class Pruebas {

	public static void main(String[] args) {	
		
		AbstractApplicationContext appCtx = new ClassPathXmlApplicationContext("Beans.xml");
		GestorClientes gc = appCtx.getBean(GestorClientes.class);
		gc.insertar("Bart Simpson");
		
		BeanQueSabeDondeVive bv = appCtx.getBean(BeanQueSabeDondeVive.class);
		
		appCtx.close();
	}
	
}

