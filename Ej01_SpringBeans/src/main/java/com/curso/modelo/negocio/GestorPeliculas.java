package com.curso.modelo.negocio;

import com.curso.modelo.entidad.Pelicula;
import com.curso.modelo.persistencia.PeliculaDao;

public class GestorPeliculas {

	private static GestorPeliculas instancia;
	
	public synchronized static GestorPeliculas getInstancia() {
		if(instancia == null) {
			instancia = new GestorPeliculas();
		}
		return instancia;
	}
	
	/////////////////////////////////////
	
	private PeliculaDao peliculaDao;
	
	private GestorPeliculas() {
		super();
	}

	public void setPeliculaDao(PeliculaDao peliculaDao) {
		this.peliculaDao = peliculaDao;
	}

	public void insertar(Pelicula pelicula){
		//LN
		//...
		System.out.println("Gestor:   " + super.toString());
		peliculaDao.insertar(pelicula);		
	}
	
}



