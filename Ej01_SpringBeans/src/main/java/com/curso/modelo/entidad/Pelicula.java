package com.curso.modelo.entidad;

public class Pelicula {

	private Integer id;
	private String titulo;
	private String genero;
	private Director m_director;
	
	// alt+shift+s c
	// alt+shift+s o
	// alt+shift+s r
	// alt+shift+s s

	private Pelicula() {
		super();
		System.out.println("Creando una película");
	}
	public Pelicula(Pelicula prototipo) {
		super();
		System.out.println("Creando una película");
		this.id = prototipo.id;
		this.titulo = prototipo.titulo;
		this.genero = prototipo.genero;
		this.m_director = prototipo.m_director;
	}

	public Pelicula(Integer id, String titulo, String genero) {
		super();
		this.id = id;
		this.titulo = titulo;
		this.genero = genero;
		System.out.println("Creando una película");
	}

	public Pelicula(Integer id, String titulo, String genero, Director x_director) {
		super();
		this.id = id;
		this.titulo = titulo;
		this.genero = genero;
		this.m_director = x_director;
		System.out.println("Creando una película");
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public Director getDirector() {
		return m_director;
	}

	public void setDirector(Director director) {
		this.m_director = director;
	}
	@Override
	public String toString() {
		return super.toString() + ": " + titulo + " (" + genero + ", " + m_director + ")";
	}
}
