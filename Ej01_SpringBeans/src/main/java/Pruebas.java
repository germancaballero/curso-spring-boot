import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.curso.modelo.entidad.Pelicula;
import com.curso.modelo.negocio.GestorPeliculas;

public class Pruebas {

	public static void main(String[] args) {
		ApplicationContext appCtx = new ClassPathXmlApplicationContext("Beans.xml");
		
		GestorPeliculas gp1 = (GestorPeliculas) appCtx.getBean("gestorPeliculas");
		GestorPeliculas gp2 = appCtx.getBean(GestorPeliculas.class);
		GestorPeliculas gp3 = appCtx.getBean(GestorPeliculas.class, "gestorPeliculas");
		
		Pelicula p = new Pelicula(null, "Alien", "CiFi");
		// Los 3 objetos en realidad son el único Bean GestorPeliculas
		gp1.insertar(p);
		gp2.insertar(p);
		gp3.insertar(p);
	}	
	
}
