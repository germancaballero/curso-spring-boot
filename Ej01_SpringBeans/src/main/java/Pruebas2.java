import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.curso.modelo.entidad.Director;
import com.curso.modelo.entidad.Pelicula;
import com.curso.modelo.negocio.GestorPeliculas;

public class Pruebas2 {

	public static void main(String[] args) {
		
		ApplicationContext appCtx = new ClassPathXmlApplicationContext("Beans2.xml");
	
		System.out.println("=========================================");
		Pelicula p1 = (Pelicula) appCtx.getBean("peliculaSingleton");
		Pelicula p2 = (Pelicula) appCtx.getBean("peliculaSingleton");
		Pelicula p3 = (Pelicula) appCtx.getBean("peliculaSingleton");
		Pelicula p4 = (Pelicula) appCtx.getBean("peliculaSingleton");
		Pelicula p5 = (Pelicula) appCtx.getBean("peliculaSingleton");
		System.out.println(p1);
		System.out.println(p2);
		System.out.println(p3);
		p3.setTitulo("CAMBIO TITULO: LA COSA");
		System.out.println(p4);
		System.out.println(p5);

		System.out.println("=========================================");
		Pelicula p6 = (Pelicula) appCtx.getBean("pelicula1");
		Pelicula p7 = (Pelicula) appCtx.getBean("pelicula2");
		Pelicula p8 = (Pelicula) appCtx.getBean("pelicula3");
		Pelicula p9 = (Pelicula) appCtx.getBean("pelicula4");
		Pelicula p10 = (Pelicula) appCtx.getBean("pelicula5");
		System.out.println(p6);
		System.out.println(p7);
		System.out.println(p8);
		System.out.println(p9);
		System.out.println(p10);
		
		System.out.println("=========================================");
		Pelicula p11 = (Pelicula) appCtx.getBean("peliculaPrototype");
		Pelicula p12 = (Pelicula) appCtx.getBean("peliculaPrototype");
		Pelicula p13 = (Pelicula) appCtx.getBean("peliculaPrototype");
		Pelicula p14 = (Pelicula) appCtx.getBean("peliculaPrototype");
		Pelicula p15 = (Pelicula) appCtx.getBean("peliculaPrototype");

		Director dir1 =  appCtx.getBean(Director.class, "director1");
		
		Pelicula p16 = new Pelicula((Pelicula) appCtx.getBean("peliculaPrototype"));
		
		System.out.println(p11);
		p12.setTitulo("CAMBIO TITULO: LA COSA");
		p12.getDirector().setNombre("Director DE LA Cosa");
		System.out.println(p12);
		System.out.println(p13);
		System.out.println(p14);
		System.out.println(p15);
	}
	
}

