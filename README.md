Spring Boot y Rest Services

Duración:                     30 horas
Perfil:                            Avanzado
Dirigido a
Esta acción formativa va dirigida a Programadores y Analistas Java. 
Requisitos previos 
Los alumnos necesitarán tener experiencia con el lenguaje de programación como en JavaEE. Recomendables conocimientos de Servlets, JSP y Web Services. Y conocimientos de Spring framework previos.
Objetivos
Este curso se centra en la los fundamentos de SpringBoot. El alumno aprenderá a desarrollar aplicaciones con SpringBoot usando todos los componentes y herramientas el que framework ofrece.
Contenido
    1. Introducción
        a. Arquitectura de microservicios
        b. Implementación con Spring Boot
        c. Groovy
    2. Spring 
    3. Instalación de Spring Boot CLI
    4. Creación e implementación de una aplicación
    5. Uso de plantillas
    6. Uso de Java con start.spring.io
    7. Starters
    8. Construcción de un JAR Ejecutable
    9. Restful Web Services con Spring MVC
        a. Clases de utilidad
        b. Peticiones complejas
        c. Respuestas del servidor
        d. Peticiones AJAX con REST y uso desde JavaScript (JSON)
        e. Componentes de la arquitectura
            i. ROA vs SOA, REST vs SOAP
            ii. Contrato del servicio. WSDL/WADL
        f. Buenas Prácticas
    10. Spring JPA
        a. Soluciones SQL
        b. Soluciones NoSQL
    11. Soporte a propiedades
    12. Desarrollo de pruebas con Spring Boot
        a. Tipos de pruebas
        b. Herramientas de testing (Mockito, Rest Assured, …)
    13. Securización de servicios con Spring Security
    14. Recolección de métricas
    15. Monitorización del servicio
    16. Empaquetamiento de servicio con Docker.
    17. Despliegue