package com.curso;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import com.curso.modelo.entidad.Cliente;
import com.curso.modelo.entidad.DetallePedido;
import com.curso.modelo.entidad.Pedido;
import com.curso.modelo.entidad.Producto;

public class CargaDatos {

	public static void main(String[] args) {
		
		ApplicationContext appCtx = SpringApplication.run(Aplicacion.class, args);

		EntityManagerFactory emf = (EntityManagerFactory) appCtx.getBean("entityManagerFactory");
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		
		Cliente c1 = new Cliente(null, "Harpo", "Su casa", "123", 1);
		Cliente c2 = new Cliente(null, "Mongomery Burns", "Su mansión", "123", 9999);
		
		em.persist(c1);
		em.persist(c2);
		
	}
	
}


