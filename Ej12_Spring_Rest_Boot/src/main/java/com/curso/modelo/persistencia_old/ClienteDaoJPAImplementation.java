package com.curso.modelo.persistencia_old;

import org.springframework.stereotype.Repository;
import com.curso.modelo.entidad.Cliente;

@Repository
public class ClienteDaoJPAImplementation extends AbstractJPADao<Cliente, Integer> implements ClienteDao {

}
