package com.curso.modelo.negocio;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.curso.modelo.entidad.Cliente;
import com.curso.modelo.persistencia.ClienteRepositorio;

// https://programmerclick.com/article/58291952160/
// https://www.baeldung.com/spring-transactional-propagation-isolation

@Service
public class GestorClientes {

	private ClienteRepositorio clienteRepo;

	public void insertar(Cliente cliente) {
		//LN...
		//clienteRepo.save(cliente);
	}

	public void modificar(Cliente cliente) {
		//LN...
		//clienteRepo.save(cliente);		
	}

	public void borrar(Cliente cliente) {
		//LN...
		//clienteRepo.delete(cliente);		
	}	

}
