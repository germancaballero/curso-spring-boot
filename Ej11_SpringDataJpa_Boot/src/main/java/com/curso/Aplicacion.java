package com.curso;

import java.util.List;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.curso.modelo.entidad.Persona;
import com.curso.modelo.persistencia.PersonaRepositorio;

@SpringBootApplication
public class Aplicacion {

	public static void main(String[] args) {

		SpringApplication.run(Aplicacion.class, args);
		System.out.println("******** INICIANDO SPRING BOOT ***********");
		
	}
	
}
